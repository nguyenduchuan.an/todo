import React from 'react';
import logo from './logo.svg';
import './Style/style.scss';
import { AddNew } from './Components/AddNew';
import { TodoList } from './Components/TodoList';
import { TodoProvider } from './Context/TodoContext';

function App() {
  return (
    <TodoProvider>
      <div className="App">
        <div className="container">
            <div className="row">
              <div className="col col-6">
              <h4>Add New Task</h4>
                <AddNew type="add" />
              </div>
              <div className="col col-6">
                <TodoList />
              </div>
            </div>
        </div>
      </div>
    </TodoProvider>
  );
}

export default App;
