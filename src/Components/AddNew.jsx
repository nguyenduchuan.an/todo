import React, { useState, useContext } from 'react'
import date from 'date-and-time';
import { v1 as uuidv1 } from 'uuid';
import { TodoContext } from '../Context/TodoContext';
const now = date.format(new Date(), 'YYYY-MM-DD');
export const AddNew = (props) => {
    const [errs, setErrs] = useState([])
    const todoContext = useContext(TodoContext)

    function useInputText(defaultValue) {
        const [value, setValue] = useState(defaultValue)
        function onChange(e) {
            setValue(e.target.value)
        }
        return {
            value,
            onChange,
        }
    }
    const useName = useInputText(props.title);
    const useDesc = useInputText(props.desc);
    const useDate = useInputText(props.date);
    const usePry = useInputText(props.piority);

    const handleSubmid = () => {
        if(!useName.value){
            setErrs(errs.concat({
                type: 'title',
                msg: 'Title is empty!'
            }))
        }else if(!useDate.value || useDate.value <= now){
            setErrs(errs.concat({
                type: 'title',
                msg: 'Date is invalid!'
            }))
        }else{
            setErrs([])
            
           
            if(props.type === 'update'){
                let newTodo = {
                    id: props.id,
                    name: useName.value,
                    desc: useDesc.value,
                    date: useDate.value,
                    piority: usePry.value
                }
                todoContext.updateTodo(newTodo, props.id)
            }else{
                let newTodo = {
                    id: uuidv1(),
                    name: useName.value,
                    desc: useDesc.value,
                    date: useDate.value,
                    piority: usePry.value
                }
                todoContext.addTodo(newTodo)
            }
        }
        
 
    }

    return (
        <div className="add-new">
            <div className="form-group">
              <input {...useName} type="text" name="" id="" className="form-control" placeholder="Add new task..." />
            </div>
            <div className="form-group">
                <label htmlFor="">Description</label>
                <textarea  {...useDesc} name="" id=""></textarea>
            </div>
            <div className="row">
                <div className="col col-6">
                    <div className="form-group">
                        <label htmlFor="">Due Date</label>
                        <input  {...useDate} className="form-control" type="date" ></input>
                    </div>
                </div>
                <div className="col col-6">
                    <div className="form-group">
                        <label htmlFor="">Piority</label>
                        <select  {...usePry} className="form-control" name="" id="" defaultValue="1">
                            <option value="0">Low</option>
                            <option value="1">Normal</option>
                            <option value="2">Hight</option>
                        </select>
                    </div>
                </div>
            </div>
            <div className="form-group">
                <button type="button" name="" id="" className="btn btn-submit"  onClick={() => handleSubmid()}>{ props.type === 'update' ? "Update" : "Add" }</button>
            </div>
            {
                errs.length > 0 ? errs.map((err, index) =>{
                    return <p key={index} className="eror">{ err.msg }</p>
                }) : ""
            }
        </div>
    )
}
