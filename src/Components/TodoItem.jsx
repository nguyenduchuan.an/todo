import React, {useState, useContext } from 'react'
import { AddNew } from './AddNew'
import { TodoContext } from '../Context/TodoContext'

export const TodoItem = (props) => {
    const [ShowDetail, setShowDetail] = useState(false)
    const todoContext = useContext(TodoContext)
    return (
        <div className="item">
            <div className="head">
                <label className="form-check-label">
                    <input type="checkbox" className="form-check-input" name="" id="" value="" onClick={e => todoContext.addBulk(props.id)}></input>
                    {props.title}
                </label>
                <div className="action">
                    <button type="button" name="" id="" className="btn btn-view" onClick={() => setShowDetail(!ShowDetail)} >Detail</button>
                    <button type="button" name="" id="" className="btn btn-delete" onClick={(() => todoContext.deleteTodo(props.id))}>Delete</button>
                </div>
            </div>
            {
             ShowDetail ? <div className="detail">
                            <AddNew title={props.title} id={props.id} desc={props.desc} date={props.date} piority={props.piority} type="update" />
                        </div> : ""
            }
        </div>
    )
}
