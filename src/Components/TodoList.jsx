import React, { useState,useContext } from 'react'
import { TodoItem } from './TodoItem'
import { TodoContext } from '../Context/TodoContext'
import { Bulk } from './Bulk'

export const TodoList = () => {
    const todoContext = useContext(TodoContext)
    const [Key, setKey] = useState('')

    const someThing = (e) => {
        if (e.keyCode === 13) {
            todoContext.searchTodo(Key)
        }
    }

    const handleChange = (e) => {
        setKey(e.target.value)
        todoContext.searchTodo(e.target.value)
    }
    return (
        <div className="todo-list">
            <h4>Todo List</h4>
            <div className="search">
                <div className="form-group">
                    <input type="text" name="" id="" className="form-control" onChange={(e) => handleChange(e)} onKeyDown={e => someThing(e)} placeholder="Search..." />
                </div>
                <div className="list">
                    {
                        todoContext.result.map((item, key) => {
                            return <TodoItem key={key} id={item.id} title={item.name} desc={item.desc} date={item.date} piority={item.piority} />
                        })
                    }
                </div>
            </div>
            { todoContext.Bulk.length > 0 ? <Bulk /> : '' }
        </div>
    )
}
