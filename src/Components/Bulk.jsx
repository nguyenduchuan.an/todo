import React, { useContext } from 'react'
import { TodoContext } from '../Context/TodoContext'

export const Bulk = () => {
    const todoContext = useContext(TodoContext)
    return (
        <div className="bulk">
           <div className="container d-flex align-content">
                <p>Bulk Action</p>
                <div className="action">
                    <button type="button" name="" id="" className="btn btn-view" >Done</button>
                    <button type="button" name="" id="" className="btn btn-delete" onClick={e => todoContext.deleteBulk()}>Delete</button>
                </div>
           </div>
        </div>
    )
}
