import React, { useState, useEffect } from 'react'

export const TodoContext = React.createContext();

export const TodoProvider = ({children}) => {
    const [todo, setTodo] = useState([])
    const [Key, setKey] = useState('')
    const [Bulk, setBulk] = useState([])
    var result = [];
    // SORT
    todo.sort(compareValues('date', 'asc')); 
    todo.map((item, key) => {
        if(item.name.indexOf(Key) !== -1){
            result.push(item)
        }
    })

    const addTodo = (item) => {
        setTodo(todo.concat(item))
    }
    function compareValues(key, order='asc') {
        return function(a, b) {
            if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            // không tồn tại tính chất trên cả hai object
                return 0; 
            }
        
            const varA = (typeof a[key] === 'string') ? 
            a[key].toUpperCase() : a[key];
            const varB = (typeof b[key] === 'string') ? 
            b[key].toUpperCase() : b[key];
        
            let comparison = 0;
            if (varA > varB) {
            comparison = 1;
            } else if (varA < varB) {
            comparison = -1;
            }
            return (
            (order == 'desc') ? (comparison * -1) : comparison
            );
        };
    }
    const addBulk = (id) => {
        if(Bulk.indexOf(id) === -1){
            setBulk(Bulk.concat(id))
        }else{
            let tempBulk = Bulk.filter(item => item != id)
            setBulk(tempBulk);
        }
        
    }
    const arrayDiff = (arr1, arr2) => {
        var diff = {};
 
        diff.arr1 = arr1.filter(function(value) {
            if (arr2.indexOf(value) === -1) {
                return value;
            }
        });
 
        diff.arr2 = arr2.filter(function(value) {
            if (arr1.indexOf(value) === -1) {
                return value;
            }
        });
 
        diff.concat = diff.arr1.concat(diff.arr2);
 
        return diff;
    };
    const deleteBulk = () => {
        let tempTodo = todo.filter(item => {
            for(let id of Bulk){
                if(item.id === id){
                    return true;
                }
            }
            return false
        })
        setBulk([])
        setTodo(arrayDiff(tempTodo, todo).concat)
    }
    const updateTodo = (items, id) => {
        let temp = todo.filter(item => item.id != id);
        setTodo(temp.concat(items))
    }
    const deleteTodo = (id) => {
        let tempTodo = todo.filter(item => item.id != id);
        let tempBulk = Bulk.filter(item => item != id)
        setTodo(tempTodo);
        setBulk(tempBulk);
    }
    const searchTodo = (key) => {
        setKey(key)
    }
    useEffect(() => {
        if(localStorage.getItem('todoData') === null){
            localStorage.setItem('todoData', todo)
        }else{
            // var temp = localStorage.getItem('todoData');
            // console.log(JSON.parse(temp))
            // setTodo(todo.concat(JSON.parse(temp)))
        }
    },[])
    return(
        <TodoContext.Provider value={{
            Bulk: Bulk,
            todo: todo,
            result: result,
            setBulk: setBulk,
            addTodo: addTodo,
            updateTodo: updateTodo,
            searchTodo: searchTodo,
            deleteTodo: deleteTodo,
            addBulk: addBulk,
            deleteBulk: deleteBulk
        }}>
            {children}
        </TodoContext.Provider>
    )
}